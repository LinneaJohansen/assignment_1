package Items;

import Exceptions.InvalidWeaponException;
import Items.*;
import Character.*;


public class Weapon extends Item implements WeaponInterface {

    //Using enumerator for weapon types
    public enum WeaponType{
        AXES,
        BOWS,
        DAGGERS,
        HAMMERS,
        STAFFS,
        SWORDS,
        WANDS
    }

    //Initializing weapon specific variables
    private double dmg;
    private double atkSpd;
    WeaponType wpn;


    //Constructor
    public Weapon(String name, int reqLvl, Attributes attr, Slot slot, double dmg, double atkSpd, WeaponType wpn) {
        super(name, reqLvl, attr, slot);
        this.dmg = dmg;
        this.atkSpd = atkSpd;
        this.wpn = wpn;
    }

    //Returns the DPS of the weapon
    public double getWeaponDPS(){
        return dmg*atkSpd;
    }

    //Method for equipping a character with a weapon. Param class, character level
    public void canEquipWeapon(CharacterClass.ClassChoices charClass, int charLvl) throws InvalidWeaponException {

        //Checks if class is valid
        if(!classEquip(charClass)){
            throw new InvalidWeaponException("Wrong class");
        }

        //Checks if level is high enough
        if(!reqLvLMet(charLvl)) {
            throw new InvalidWeaponException("Not high enough level");
        }
    }

    //Method for checking that player class can equip weapon
    public boolean classEquip(CharacterClass.ClassChoices charClass) {
        //Checks validity
        if (charClass == CharacterClass.ClassChoices.MAGE && (wpn == WeaponType.STAFFS || wpn == WeaponType.WANDS)) {
            return true;
        } else if (charClass == CharacterClass.ClassChoices.RANGER && wpn == WeaponType.BOWS){
            return true;
        } else if (charClass == CharacterClass.ClassChoices.ROGUE && (wpn == WeaponType.DAGGERS || wpn == WeaponType.SWORDS)) {
            return true;
        } else if (charClass == CharacterClass.ClassChoices.WARRIOR && (wpn == WeaponType.AXES || wpn == WeaponType.SWORDS || wpn == WeaponType.HAMMERS)) {
            return true;
        }
            return false;
        }

}
