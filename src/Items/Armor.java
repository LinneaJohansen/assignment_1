package Items;
import Character.*;
import Exceptions.*;

public class Armor extends Item implements ArmorInterface{

    //Enumeration for different armortypes
    public enum ArmorType {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }

    //Initializing variables
    ArmorType arm;

    //Constructor
    public Armor(String name, int reqLvl, Attributes attr, Slot slot, ArmorType arm) {
        super(name, reqLvl, attr, slot);
        this.arm=arm;
    }

    //Method for equipping a character with armor. Param class, character level
    public void canEquipArmor(CharacterClass.ClassChoices charClass, int charLvl) throws InvalidArmorException {

        //Checks if class is valid
        if(!classEquip(charClass)){
            throw new InvalidArmorException("Wrong class");
        }

        //Checks if level is high enough
        if(!reqLvLMet(charLvl)) {
            throw new InvalidArmorException("Not high enough level");
        }
    }

    //Method for checking that player class can equip armor
    public boolean classEquip(CharacterClass.ClassChoices charClass) {

        //Checks validity
        if (charClass == CharacterClass.ClassChoices.MAGE && arm == ArmorType.CLOTH) {
            return true;
        } else if (charClass == CharacterClass.ClassChoices.RANGER && (arm == ArmorType.LEATHER || arm == ArmorType.MAIL)){
            return true;
        } else if (charClass == CharacterClass.ClassChoices.ROGUE && (arm == ArmorType.LEATHER || arm == ArmorType.MAIL)) {
            return true;
        } else if (charClass == CharacterClass.ClassChoices.WARRIOR && (arm == ArmorType.MAIL || arm == ArmorType.PLATE)) {
            return true;
        }
        return false;
    }
}
