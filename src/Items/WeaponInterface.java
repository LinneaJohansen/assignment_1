package Items;

import Exceptions.InvalidWeaponException;

public interface WeaponInterface {
    //Demands that there is a check to see if player class can equip weapon
    public boolean classEquip(Character.CharacterClass.ClassChoices param);

    //Demands that there is a method to throw exception if invalid equipment is equipped
    public void canEquipWeapon(Character.CharacterClass.ClassChoices charClass, int charLvl) throws InvalidWeaponException;
}
