package Items;

import Exceptions.InvalidArmorException;

public interface ArmorInterface {
    //Demands that there is a check to see if player class can equip armor
    public boolean classEquip(Character.CharacterClass.ClassChoices param);

    //Demands that there is a method to throw exception if invalid equipment is equipped
    public void canEquipArmor(Character.CharacterClass.ClassChoices charClass, int charLvl) throws InvalidArmorException;
}
