package Items;
import Character.*;

public abstract class Item {
    //Enumerator for item slot
    public enum Slot {
        WEAPON,
        HEAD,
        BODY,
        LEGS
    }

    //Initializing variables
    String name;
    int reqLvl;
    Slot slot;
    public Attributes attr;

    //Constructor
    public Item(String name, int reqLvl, Attributes attr, Slot slot) {
        this.name = name;
        this.reqLvl = reqLvl;
        this.attr = attr;
        this.slot = slot;
    }

    //Method for checking if player has met required level
    public boolean reqLvLMet(int level) {
        if(level >= reqLvl) return true;
        return false;
    }

    //Method to return name of item
    public String getName(){
        return name;
    }

    //Method to return the slot of item
    public Slot getSlot(){
        return slot;
    }

    //Method to get the stats on the item
    public Attributes getAttr() {
        return attr;
    }

    //Method to return the required level to equip the item
    public int getReqLvl(){
        return reqLvl;
    }
}
