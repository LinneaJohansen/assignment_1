package Exceptions;

public class InvalidWeaponException extends Exception {
    //Sets custom exception
    public InvalidWeaponException(String message) {
        super(message);
    }
}
