package Exceptions;

public class InvalidArmorException extends Exception {
    //Sets custom exception
    public InvalidArmorException(String message) {
        super(message);
    }
}