import Items.*;
import Character.*;

public class Main {
    public static void main(String[] args) {
        PlayerCharacter mage = new PlayerCharacter("Gandalf", CharacterClass.ClassChoices.MAGE, 1);
        PlayerCharacter ranger = new PlayerCharacter("Legolas", CharacterClass.ClassChoices.RANGER, 1);
        PlayerCharacter rogue = new PlayerCharacter("Pippin", CharacterClass.ClassChoices.ROGUE, 1);
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);

        Weapon testAxe = new Weapon("Common Axe", 1,
                new Attributes(0,0,0,0), Item.Slot.WEAPON, 7,1.1, Weapon.WeaponType.AXES);

        Armor testPlateBody = new Armor("Common Plate Body Armor", 1,
                new Attributes(1,0,0,2), Item.Slot.BODY, Armor.ArmorType.CLOTH);

        warrior.printChar();

        warrior.equipItem(testAxe);
        warrior.equipItem(testPlateBody);

        warrior.printChar();
    }
}




