package Character;
import Items.*;
import Exceptions.*;
import java.util.HashMap;

public class PlayerCharacter extends CharacterClass implements PlayerCharacterInterface {

    //Constructor
    public PlayerCharacter(String name, CharacterClass.ClassChoices chosenClass , int level) {
        super(name, chosenClass, level);
    }

    //Creates a HashMap with the current equipped items
    HashMap<Item.Slot, Item> equippedItems = new HashMap<Item.Slot, Item>();

    //Method for levelling up a character
    public void levelUp(){
        System.out.println("Ding!");
        level++;

        //Updating attributes to match new level
        attr.calcBaseStats(chosenClass,level);
        Attributes tempAttr = new Attributes(attr.getStats()[0], attr.getStats()[1], attr.getStats()[2], attr.getStats()[3]);
        attr=tempAttr;
    }

    public int getLevel(){
        return level;
    }

    //Method for printing equipped items
    public void printItems(){
        if(equippedItems.get("Items.Weapon")!= null) System.out.println(equippedItems.get(Item.Slot.WEAPON).getName());
        if(equippedItems.get("Head")!= null) System.out.println(equippedItems.get(Item.Slot.HEAD).getName());
        if(equippedItems.get("Body")!= null) System.out.println(equippedItems.get(Item.Slot.BODY).getName());
        if(equippedItems.get("Legs")!= null) System.out.println(equippedItems.get(Item.Slot.LEGS).getName());
    }

    //Checks if weapon is equipped and replaces it, if no weapon is equipped it puts current item into weapon slot
    public void equipWeapon(Weapon toBeEquipped) {
        if(equippedItems.get(Item.Slot.WEAPON) == null) {
            equippedItems.put(Item.Slot.WEAPON, toBeEquipped);
        } else {equippedItems.replace(Item.Slot.WEAPON, toBeEquipped);}

        //Update stats for equipping
        totalStats = attr.getTotalStats(equippedItems);
    }

    //Checks if slot is equipped and replaces item, if slot is empty it puts the item to the assigned slot
    public void equipArmor(Item.Slot slot, Armor toBeEquipped) {
        if((equippedItems.get(slot) == null)) {
            equippedItems.put(slot, toBeEquipped);
        } else {equippedItems.replace(slot, toBeEquipped);}
        equippedItems.replace(slot,toBeEquipped);
        System.out.println("Equipped "+equippedItems.get(slot).getName()+".");
        //Update stats for equipping
        totalStats = attr.getTotalStats(equippedItems);
    }

    //Method for equipping items. Checking item types and calling the appropriate methods to see if player requirements are met
    public void equipItem(Item item){
        //Checks if the equipped item is a weapon
        if(item.getSlot() == Item.Slot.WEAPON) {
            Weapon wpn = (Weapon) item;
            try {
                wpn.canEquipWeapon(chosenClass, level);
                equipWeapon(wpn);
            } catch (InvalidWeaponException e) {
                System.out.println(e);
            }
        } else {
            Armor arr = (Armor) item;
            try {
                arr.canEquipArmor(chosenClass, level);
                equipArmor(item.getSlot(), arr);
            } catch (InvalidArmorException e) {
                System.out.println(e);
            }
        }
    }

    public HashMap<Item.Slot, Item> getEquipment(){
        return equippedItems;
    }

    public int[] getTotalStats(){
        return attr.getTotalStats(equippedItems);
    }

    //Method for calculating the DPS of the character
    public double getDPS(){
        double wpnDPS;
        if(equippedItems.get(Item.Slot.WEAPON) == null) {
            //Sets DPS to 1 on no weapon equipped
            wpnDPS=1;
        } else {
            //Gets the DPS of the equipped weapon
            Item tempItem = equippedItems.get(Item.Slot.WEAPON);
            Weapon tempWpn= (Weapon) tempItem;
            wpnDPS = tempWpn.getWeaponDPS();
        }

        //Updates DPS according to class
        wpnDPS = wpnDPS*(1+attr.getPrimaryStat(chosenClass)/100);
        return wpnDPS;
    }

    //Stringbuilder for printing out character information
    public void printChar(){
        StringBuilder output = new StringBuilder();

        //Formatting information
        output.append("Character name: " +name);
        output.append("\n");
        output.append("Character level: " +level);
        output.append("\n");
        output.append("Strength: " +attr.getStats()[0]);
        output.append("\n");
        output.append("Dexterity: " +attr.getStats()[1]);
        output.append("\n");
        output.append("Intelligence: " +attr.getStats()[2]);
        output.append("\n");
        output.append("Vitality: " +attr.getStats()[3]);
        output.append("\n");
        output.append("DPS: " +getDPS());

        //Printing
        System.out.println(output.toString());
    }
}
