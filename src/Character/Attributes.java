package Character;

import Items.Armor;
import Items.Item;
import Items.Weapon;

import java.util.HashMap;

public class Attributes {

        //Initializing stats
        int str;
        int dex;
        int intellect;
        int vit;

        //Constructor
        public Attributes (int str, int dex, int intellect, int vit){
            this.str = str;
            this.dex = dex;
            this.intellect = intellect;
            this.vit = vit;
        }

        //Creating an array to store stats
        int [] attr = {str, dex, intellect, vit};

        //Method for calculating appropriate base stats for each character class
        public void calcBaseStats(CharacterClass.ClassChoices charClass, int level) {
            //Base scaling
            str = 1*level;
            dex = 1*level;
            intellect = 1*level;

            //Modifying stats by level and class
            switch (charClass){
                case MAGE:intellect = 3+5*level; break;
                case RANGER:dex = 2+5*level; break;
                case ROGUE:str=1+1*level; dex=2+4*level; break;
                case WARRIOR:str=2+3*level; dex=2*level; break;
                default: System.out.println("Invalid class"); break;
            }
        }

        public int[] getStats(){
            return new int[]{str, dex, intellect, vit};
        }

        public double getPrimaryStat(CharacterClass.ClassChoices charClass){
            switch(charClass) {
                //Returns intellect for mages
                case MAGE: return intellect;

                //Returns dex for rangers
                case RANGER: return dex;

                //Returns dex for rogues
                case ROGUE: return dex;

                //Returns str for warriors
                case WARRIOR: return str;
                default: return 0;
            }
        }

    //Get stats based on base stats and equipment
    public int[] getTotalStats(HashMap<Item.Slot, Item> equippedItems){

        //Makes placeholders to check if item exists
        Weapon tempWpn = (Weapon)equippedItems.get(Item.Slot.WEAPON);
        Armor tempHead = (Armor)equippedItems.get(Item.Slot.HEAD);
        Armor tempBody = (Armor)equippedItems.get(Item.Slot.BODY);
        Armor tempLegs = (Armor)equippedItems.get(Item.Slot.LEGS);

        //Updates strength according to gear
        if (tempWpn != null) {
            str += tempWpn.getAttr().getStats()[0];
            dex += tempWpn.getAttr().getStats()[1];
            intellect += tempWpn.getAttr().getStats()[2];
            vit += tempWpn.getAttr().getStats()[3];
        }

        //Updates dexterity according to gear
        if (tempHead != null) {
            str += tempHead.getAttr().getStats()[0];
            dex += tempHead.getAttr().getStats()[1];
            intellect += tempHead.getAttr().getStats()[2];
            vit += tempHead.getAttr().getStats()[3];
        }

        //Updates intellect according to gear
        if (tempBody != null) {
            str += tempBody.getAttr().getStats()[0];
            dex += tempBody.getAttr().getStats()[1];
            intellect += tempBody.getAttr().getStats()[2];
            vit += tempBody.getAttr().getStats()[3];
        }

        //Updates vitality according to gear
        if (tempLegs != null) {
            str += tempLegs.getAttr().getStats()[0];
            dex += tempLegs.getAttr().getStats()[1];
            intellect += tempLegs.getAttr().getStats()[2];
            vit += tempLegs.getAttr().getStats()[3];
        }

        int[] totalStats = {str, dex, intellect, vit};
        return totalStats;
    }

    public void printAttr(){
        System.out.println(str);
        System.out.println(dex);
        System.out.println(intellect);
        System.out.println(vit);
    }

    public String getAttrString(){
            StringBuilder sb = new StringBuilder();
            sb.append("Str: "+str+"\n");
            sb.append("Dex: "+dex+"\n");
            sb.append("Int: "+intellect+"\n");
            sb.append("Vit: "+vit);
            String returnString = sb.toString();
            return returnString;
    }
}
