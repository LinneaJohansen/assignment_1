package Character;

import Items.*;
import Exceptions.*;
import java.util.HashMap;

public abstract class CharacterClass {
    //Using enumerator for class choice
    public enum ClassChoices {
        MAGE,
        RANGER,
        ROGUE,
        WARRIOR
    }

    //Initializing variables
    ClassChoices chosenClass;
    String name;
    int level;
    public int[] statTemplate = new int[]{1,1,1,1};
    public int[] totalStats = new int[]{};
    Attributes attr = new Attributes(1,1,1,1);

    //Constructor
    public CharacterClass(String name, ClassChoices chosenClass, int level) {
        this.name = name;
        this.chosenClass = chosenClass;
        this.level = level;

        //Initializing attributes
        attr.calcBaseStats(chosenClass,level);
        Attributes tempAttr = new Attributes(attr.getStats()[0], attr.getStats()[1], attr.getStats()[2], attr.getStats()[3]);
        attr=tempAttr;
    }

    public Attributes getAttr(){
        return attr;
    }
}
