package Character;

import Items.Item;

public interface PlayerCharacterInterface {
    //Demands players to be able to level up
    public void levelUp();

    //Demands players be able to equip items
    public void equipItem(Item item);

    //Demands being able to get DPS values
    public double getDPS();
}
