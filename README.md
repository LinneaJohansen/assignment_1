## Assignment 1
#### Linnea Ramstad Johansen

### Table of contents
* [Introduction](#introduction)
* [Overview](#overview)
* [Functions and methods](#functions-and-methods)
* [Unit tests](#unit-tests)
* [Directory](#directory)
* [How to download](#how-to-download)


### Introduction

This program is an RPG simulation and aims at letting the user create, 
level and equip characters.

### Overview

The game lets the user create characters from the following four classes:
* Mage
* Ranger
* Rogue
* Warrior

The different classes start with different distributions of stats and 
they each gain more DPS from their primary attribute (main stat). The
stats implemented in this simulation are
* Strength
* Dexterity
* Intelligence
* Vitality

The user may also equip a character in accord to what class and level the
character is (F.ex a mage may not equip certain items such as axes or
plate armor). The DPS of a character (damage per second) is calculated using
their attributes and weapon DPS.

### Functions and methods
The program contains different classes, both abstract 'hub' classes
and children classes expanding upon them.

The CharacterClass abstract class lays the fundamentals for each character,
storing their chosen class in an enumerator. The PlayerCharacter class expands
upon this class to store information more unique to this certain character,
such as their items, name and stats. The methods for equipping weapons and
armor are also stored within the PlayerCharacter class.

The attributes class is key to calculating the DPS and stats for different
characters. Each attribute object stores information about the different
stats and are frequently updated whenever a character levels up or puts
on equipment.

The item abstract class serves as the parent class for weapons and armor.
The parent class contains general information (name, attributes, etc) whereas
the more focused classes such as weapon hold information about the weapon DPS,
weapon type, etc. Checks to see if a character can actually use the equipment
are stored within these classes as well.

Exceptions contain custom exceptions that will alert the user if invalid
armor or weapons have been equipped.

### Unit tests

The program includes a plethora of unit tests to see that each function behaves
as expected. These are split into three main classes; CharacterTests, AttributeTests
and ItemTests. Each of these classes checks that methods related to the respected 
class does what it is supposed to.

### Directory
```
Assignment 1
| README.md
| .gitignore
| Assignment_iml
|___ src
|      | Main.java
|      |___ Character 
|      |            | Attributes.java 
|      |            | CharacterClass.java
|      |            | PlayerCharacter.java
|      |            | PlayerCharacterInterface.java
|      |            
|      |___ Exceptions            
|      |            | InvalidArmorException.java
|      |            | InvalidWeaponException.java
|      |___ Items            
|      |            | Armor.java
|      |            | ArmorInterface.java
|      |            | Weapon.java
|      |            | WeaponInterface.java
|      |            | Item.java
|___ tests
|      |___ Character
|      |            | AttributesTest.java
|      |            | CharacterClassTest.java
|      |___ Items
|      |            | ItemTest.java 

```
### How to download

`Create an SSH key and clone the following repo`
`git@gitlab.com:LinneaJohansen/assignment_1.git`

`Open Intellij`

## Author

Linnea Ramstad Johansen