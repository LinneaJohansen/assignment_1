package Items;

import static org.junit.jupiter.api.Assertions.*;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import Items.*;
import Character.Attributes.*;
import Character.*;

import java.util.HashMap;


class ItemTest {

    //              WEAPON TESTS

    //Test to see if character equips weapon properly
    @Test
    public void characterEquip_validEquipment_returnSameName(){
        //Arrange
        Weapon testAxe = new Weapon("Common Axe", 1,
                new Attributes(0,0,0,0), Item.Slot.WEAPON, 7,1.1, Weapon.WeaponType.AXES);
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);
        HashMap<Item.Slot, Item> testInventory = new HashMap<Item.Slot, Item>();
        testInventory.put(Item.Slot.WEAPON, testAxe);
        String expected = testInventory.get(Item.Slot.WEAPON).getName();

        //Act
        warrior.equipItem(testAxe);
        String actual =warrior.getEquipment().get(Item.Slot.WEAPON).getName();

        //Assert
        assertEquals(expected, actual);
    }

    //Test if character is allowed to equip valid weapon
    @Test
    public void classEquip_validWeapon_returnTrue(){
        //Arrange
        boolean expected = true;
        Weapon testAxe = new Weapon("Common Axe", 1,
                new Attributes(0,0,0,0), Item.Slot.WEAPON, 7,1.1, Weapon.WeaponType.AXES);

        //Act
        boolean actual = testAxe.classEquip(CharacterClass.ClassChoices.WARRIOR);

        //Assert
        assertEquals(expected,actual);
    }

    //Test if character is allowed to equip invalid weapon
    @Test
    public void classEquip_invalidWeapon_returnFalse(){
        //Arrange
        boolean expected = false;
        Weapon testAxe = new Weapon("Common Axe", 1,
                new Attributes(0,0,0,0), Item.Slot.WEAPON, 7,1.1, Weapon.WeaponType.AXES);

        //Act
        boolean actual = testAxe.classEquip(CharacterClass.ClassChoices.MAGE);

        //Assert
        assertEquals(expected,actual);
    }

    //Test if character is allowed to equip valid equipment for level
    @Test
    public void charEquip_validReqLvlWeapon_returnTrue(){
        //Arrange
        boolean expected = true;
        Weapon testAxe = new Weapon("Common Axe", 1,
                new Attributes(0,0,0,0), Item.Slot.WEAPON, 7,1.1, Weapon.WeaponType.AXES);

        //Act
        boolean actual = testAxe.reqLvLMet(1);

        //Assert
        assertEquals(expected,actual);
    }

    //Test if character is allowed to equip invalid equipment for level
    @Test
    public void charEquip_invalidReqLvlWeapon_returnFalse(){
        //Arrange
        boolean expected = false;
        Weapon testAxe = new Weapon("Common Axe", 3,
                new Attributes(0,0,0,0), Item.Slot.WEAPON, 7,1.1, Weapon.WeaponType.AXES);

        //Act
        boolean actual = testAxe.reqLvLMet(1);

        //Assert
        assertEquals(expected,actual);
    }


    //Test if InvalidWeaponException for wrong class is thrown when invalid weapon is equipped
    @Test
    public void classEquip_invalidWeaponExceptionThrown_returnTrue(){
        //Arrange
        String expected = "Wrong class";
        Weapon testAxe = new Weapon("Common Axe", 1,
                new Attributes(0,0,0,0), Item.Slot.WEAPON, 7,1.1, Weapon.WeaponType.AXES);

        //Act
        Exception e = assertThrows(InvalidWeaponException.class, () ->
                testAxe.canEquipWeapon(CharacterClass.ClassChoices.MAGE,1));
        String actual = e.getMessage();

        //Assert
        assertEquals(expected,actual);
    }

    //Test if InvalidWeaponException for wrong level is thrown when invalid weapon is equipped
    @Test
    public void charEquip_invalidWeaponExceptionThrown_returnTrue(){
        //Arrange
        String expected = "Not high enough level";
        Weapon testAxe = new Weapon("Common Axe", 3,
                new Attributes(0,0,0,0), Item.Slot.WEAPON, 7,1.1, Weapon.WeaponType.AXES);

        //Act
        Exception e = assertThrows(InvalidWeaponException.class, () ->
                testAxe.canEquipWeapon(CharacterClass.ClassChoices.WARRIOR,1));
        String actual = e.getMessage();

        //Assert
        assertEquals(expected,actual);
    }

    //              ARMOR TESTS


    //Test if character is allowed to equip valid weapon
    @Test
    public void classEquip_validArmor_returnTrue(){
        //Arrange
        boolean expected = true;
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1,
                new Attributes(1,0,0,2), Item.Slot.BODY, Armor.ArmorType.PLATE);

        //Act
        boolean actual = testPlateBody.classEquip(CharacterClass.ClassChoices.WARRIOR);

        //Assert
        assertEquals(expected,actual);
    }

    //Test if character is allowed to equip invalid weapon
    @Test
    public void classEquip_invalidArmor_returnFalse(){
        //Arrange
        boolean expected = false;
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1,
                new Attributes(1,0,0,2), Item.Slot.BODY, Armor.ArmorType.PLATE);
        //Act
        boolean actual = testPlateBody.classEquip(CharacterClass.ClassChoices.MAGE);

        //Assert
        assertEquals(expected,actual);
    }

    //Test if character is allowed to equip valid equipment for level
    @Test
    public void charEquip_validReqLvlArmor_returnTrue(){
        //Arrange
        boolean expected = true;
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1,
                new Attributes(1,0,0,2), Item.Slot.BODY, Armor.ArmorType.PLATE);
        //Act
        boolean actual = testPlateBody.reqLvLMet(1);

        //Assert
        assertEquals(expected,actual);
    }

    //Test if character is allowed to equip invalid equipment for level
    @Test
    public void charEquip_invalidReqLvlArmor_returnFalse(){
        //Arrange
        boolean expected = false;
        Armor testPlateBody = new Armor("Common Plate Body Armor", 3,
                new Attributes(1,0,0,2), Item.Slot.BODY, Armor.ArmorType.PLATE);
        //Act
        boolean actual = testPlateBody.reqLvLMet(1);

        //Assert
        assertEquals(expected,actual);
    }

    //Test if InvalidArmorException for wrong class is thrown when invalid armor is equipped
    @Test
    public void classEquip_invalidArmorExceptionThrown_returnTrue(){
        //Arrange
        String expected = "Wrong class";
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1,
                new Attributes(1,0,0,2), Item.Slot.BODY, Armor.ArmorType.PLATE);


        //Act
        Exception e = assertThrows(InvalidArmorException.class, () ->
                testPlateBody.canEquipArmor(CharacterClass.ClassChoices.MAGE,1));
        String actual = e.getMessage();

        //Assert
        assertEquals(expected,actual);
    }

    //Test if InvalidArmorException for wrong level is thrown when invalid armor is equipped
    @Test
    public void charEquip_invalidArmorExceptionThrown_returnTrue(){
        //Arrange
        String expected = "Not high enough level";
        Armor testPlateBody = new Armor("Common Plate Body Armor", 3,
                new Attributes(1,0,0,2), Item.Slot.BODY, Armor.ArmorType.PLATE);


        //Act
        Exception e = assertThrows(InvalidArmorException.class, () ->
                testPlateBody.canEquipArmor(CharacterClass.ClassChoices.WARRIOR,1));
        String actual = e.getMessage();

        //Assert
        assertEquals(expected,actual);
    }

}