package Character;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AttributesTest {
    //          STRENGTH CHECKS

    //Tests strength for mage
    @Test
    public void testInitialMageStats_createMage_returnLevelOneStrengthAttribute(){
        //Arrange
        PlayerCharacter mage = new PlayerCharacter("Gandalf", CharacterClass.ClassChoices.MAGE, 1);
        Attributes expectedAttr = new Attributes(1,1,8,1);
        int expected = expectedAttr.getStats()[0];

        //Act
        Attributes actualAttr = mage.getAttr();
        int actual = actualAttr.getStats()[0];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests strength for ranger
    @Test
    public void testInitialRangerStats_createRanger_returnLevelOneStrengthAttribute(){
        //Arrange
        PlayerCharacter ranger = new PlayerCharacter("Legolas", CharacterClass.ClassChoices.RANGER, 1);
        Attributes expectedAttr = new Attributes(1,7,1,1);
        int expected = expectedAttr.getStats()[0];

        //Act
        Attributes actualAttr = ranger.getAttr();
        int actual = actualAttr.getStats()[0];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests strength for rogue
    @Test
    public void testInitialRogueStats_createRogue_returnLevelOneStrengthAttribute(){
        //Arrange
        PlayerCharacter rogue = new PlayerCharacter("Pippin", CharacterClass.ClassChoices.ROGUE, 1);
        Attributes expectedAttr = new Attributes(2,6,1,1);
        int expected = expectedAttr.getStats()[0];

        //Act
        Attributes actualAttr = rogue.getAttr();
        int actual = actualAttr.getStats()[0];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests strength for warrior
    @Test
    public void testInitialWarriorStats_createWarrior_returnLevelOneStrengthAttribute(){
        //Arrange
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);
        Attributes expectedAttr = new Attributes(5,2,1,1);
        int expected = expectedAttr.getStats()[0];

        //Act
        Attributes actualAttr = warrior.getAttr();
        int actual = actualAttr.getStats()[0];

        //Assert
        assertEquals(expected,actual);
    }

    //          DEXTERITY CHECKS

    //Tests dexterity for mage
    @Test
    public void testInitialMageStats_createMage_returnLevelOneDexterityAttribute(){
        //Arrange
        PlayerCharacter mage = new PlayerCharacter("Gandalf", CharacterClass.ClassChoices.MAGE, 1);
        Attributes expectedAttr = new Attributes(1,1,8,1);
        int expected = expectedAttr.getStats()[1];

        //Act
        Attributes actualAttr = mage.getAttr();
        int actual = actualAttr.getStats()[1];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests dexterity for ranger
    @Test
    public void testInitialRangerStats_createRanger_returnLevelOneDexterityAttribute(){
        //Arrange
        PlayerCharacter ranger = new PlayerCharacter("Legolas", CharacterClass.ClassChoices.RANGER, 1);
        Attributes expectedAttr = new Attributes(1,7,1,1);
        int expected = expectedAttr.getStats()[1];

        //Act
        Attributes actualAttr = ranger.getAttr();
        int actual = actualAttr.getStats()[1];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests dexterity for rogue
    @Test
    public void testInitialRogueStats_createRogue_returnLevelOneDexterityAttribute(){
        //Arrange
        PlayerCharacter rogue = new PlayerCharacter("Pippin", CharacterClass.ClassChoices.ROGUE, 1);
        Attributes expectedAttr = new Attributes(2,6,1,1);
        int expected = expectedAttr.getStats()[1];

        //Act
        Attributes actualAttr = rogue.getAttr();
        int actual = actualAttr.getStats()[1];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests dexterity for warrior
    @Test
    public void testInitialWarriorStats_createWarrior_returnLevelOneDexterityAttribute(){
        //Arrange
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);
        Attributes expectedAttr = new Attributes(5,2,1,1);
        int expected = expectedAttr.getStats()[1];

        //Act
        Attributes actualAttr = warrior.getAttr();
        int actual = actualAttr.getStats()[1];

        //Assert
        assertEquals(expected,actual);
    }

    //          INTELLIGENCE CHECKS

    //Tests intelligence for mage
    @Test
    public void testInitialMageStats_createMage_returnLevelOneIntelligenceAttribute(){
        //Arrange
        PlayerCharacter mage = new PlayerCharacter("Gandalf", CharacterClass.ClassChoices.MAGE, 1);
        Attributes expectedAttr = new Attributes(1,1,8,1);
        int expected = expectedAttr.getStats()[2];

        //Act
        Attributes actualAttr = mage.getAttr();
        int actual = actualAttr.getStats()[2];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests intelligence for ranger
    @Test
    public void testInitialRangerStats_createRanger_returnLevelOneIntelligenceAttribute(){
        //Arrange
        PlayerCharacter ranger = new PlayerCharacter("Legolas", CharacterClass.ClassChoices.RANGER, 1);
        Attributes expectedAttr = new Attributes(1,7,1,1);
        int expected = expectedAttr.getStats()[2];

        //Act
        Attributes actualAttr = ranger.getAttr();
        int actual = actualAttr.getStats()[2];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests dexterity for rogue
    @Test
    public void testInitialRogueStats_createRogue_returnLevelOneIntelligenceAttribute(){
        //Arrange
        PlayerCharacter rogue = new PlayerCharacter("Pippin", CharacterClass.ClassChoices.ROGUE, 1);
        Attributes expectedAttr = new Attributes(2,6,1,1);
        int expected = expectedAttr.getStats()[2];

        //Act
        Attributes actualAttr = rogue.getAttr();
        int actual = actualAttr.getStats()[2];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests intelligence for warrior
    @Test
    public void testInitialWarriorStats_createWarrior_returnLevelOneIntelligenceAttribute(){
        //Arrange
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);
        Attributes expectedAttr = new Attributes(5,2,1,1);
        int expected = expectedAttr.getStats()[2];

        //Act
        Attributes actualAttr = warrior.getAttr();
        int actual = actualAttr.getStats()[2];

        //Assert
        assertEquals(expected,actual);
    }

    //          VITALITY CHECKS

    //Tests vitality for mage
    @Test
    public void testInitialMageStats_createMage_returnLevelOneVitalityAttribute(){
        //Arrange
        PlayerCharacter mage = new PlayerCharacter("Gandalf", CharacterClass.ClassChoices.MAGE, 1);
        Attributes expectedAttr = new Attributes(1,1,8,1);
        int expected = expectedAttr.getStats()[3];

        //Act
        Attributes actualAttr = mage.getAttr();
        int actual = actualAttr.getStats()[3];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests vitality for ranger
    @Test
    public void testInitialRangerStats_createRanger_returnLevelOneVitalityAttribute(){
        //Arrange
        PlayerCharacter ranger = new PlayerCharacter("Legolas", CharacterClass.ClassChoices.RANGER, 1);
        Attributes expectedAttr = new Attributes(1,7,1,1);
        int expected = expectedAttr.getStats()[3];

        //Act
        Attributes actualAttr = ranger.getAttr();
        int actual = actualAttr.getStats()[3];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests vitality for rogue
    @Test
    public void testInitialRogueStats_createRogue_returnLevelOneVitalityAttribute(){
        //Arrange
        PlayerCharacter rogue = new PlayerCharacter("Pippin", CharacterClass.ClassChoices.ROGUE, 1);
        Attributes expectedAttr = new Attributes(2,6,1,1);
        int expected = expectedAttr.getStats()[3];

        //Act
        Attributes actualAttr = rogue.getAttr();
        int actual = actualAttr.getStats()[3];

        //Assert
        assertEquals(expected,actual);
    }

    //Tests vitality for warrior
    @Test
    public void testInitialWarriorStats_createWarrior_returnLevelOneVitalityAttribute(){
        //Arrange
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);
        Attributes expectedAttr = new Attributes(5,2,1,1);
        int expected = expectedAttr.getStats()[3];

        //Act
        Attributes actualAttr = warrior.getAttr();
        int actual = actualAttr.getStats()[3];

        //Assert
        assertEquals(expected,actual);
    }

}