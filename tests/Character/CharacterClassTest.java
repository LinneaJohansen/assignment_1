package Character;

import Items.Armor;
import Items.Item;
import Items.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterClassTest {

    //Testing to see that a character is level 1 when created
    @Test
    public void newCharacter_isLevelOne_returnTrue(){
        //Arrange
        int expected = 1;
        PlayerCharacter testChar = new PlayerCharacter("Gandalf", CharacterClass.ClassChoices.MAGE, 1);

        //Act
        int actual = testChar.getLevel();

        //Assert
        assertEquals(expected, actual);
    }

    //Test to see that character levels up appropriately on levelUp()
    @Test
    public void levelUp_levelOneChar_returnIncrementalLevelIncrease(){
        //Arrange
        PlayerCharacter testChar = new PlayerCharacter("Gandalf", CharacterClass.ClassChoices.MAGE, 1);
        int expected = 2;

        //Act
        testChar.levelUp();;
        int actual = testChar.getLevel();

        //Asserts
        assertEquals(expected, actual);
    }

    //Testing to see that each character class is created properly (tests for each attribute can be found in
    //AttributeTests
    //Tests for mage
    @Test
    public void testInitialMageStats_createMage_returnLevelOneAttributes() {
        //Arrange
        PlayerCharacter mage = new PlayerCharacter("Gandalf", CharacterClass.ClassChoices.MAGE, 1);
        String expectedStr = "Str: " + 1 + "\n" + "Dex: " + 1 + "\n" + "Int: " + 8 + "\n" + "Vit: " + 1;

        //Act
        Attributes actualAttr = mage.getAttr();
        String actualStr = actualAttr.getAttrString();

        //Assert
        assertEquals(expectedStr, actualStr);

    }

    //Tests for ranger
    @Test
    public void testInitialRangerStats_createRanger_returnLevelOneAttributes(){
        //Arrange
        PlayerCharacter ranger = new PlayerCharacter("Legolas", CharacterClass.ClassChoices.RANGER, 1);
        String expectedStr = "Str: "+1+"\n"+"Dex: "+7+"\n"+"Int: "+1+"\n"+"Vit: "+1;

        //Act
        Attributes actualAttr = ranger.getAttr();
        String actualStr = actualAttr.getAttrString();

        //Assert
        assertEquals(expectedStr, actualStr);
    }

    //Tests for rogue
    @Test
    public void testInitialRogueStats_createRogue_returnLevelOneAttributes(){
        //Arrange
        PlayerCharacter rogue = new PlayerCharacter("Pippin", CharacterClass.ClassChoices.ROGUE, 1);
        String expectedStr = "Str: "+2+"\n"+"Dex: "+6+"\n"+"Int: "+1+"\n"+"Vit: "+1;

        //Act
        Attributes actualAttr = rogue.getAttr();
        String actualStr = actualAttr.getAttrString();

        //Assert
        assertEquals(expectedStr, actualStr);
    }

    //Tests for warrior
    @Test
    public void testInitialWarriorStats_createWarrior_returnLevelOneAttributes(){
        //Arrange
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);
        String expectedStr = "Str: "+5+"\n"+"Dex: "+2+"\n"+"Int: "+1+"\n"+"Vit: "+1;

        //Act
        Attributes actualAttr = warrior.getAttr();
        String actualStr = actualAttr.getAttrString();

        //Assert
        assertEquals(expectedStr, actualStr);
    }

    //Tests to see that attributes are properly increased when levelling for each class

    //Test for mage
    @Test
    public void testMageLevelUpStats_levelMage_returnEqualAttributes(){
        //Arrange
        PlayerCharacter mage = new PlayerCharacter("Gandalf", CharacterClass.ClassChoices.MAGE, 1);
        String expectedStr = "Str: "+2+"\n"+"Dex: "+2+"\n"+"Int: "+13+"\n"+"Vit: "+1;

        //Act
        mage.levelUp();
        Attributes actualAttr = mage.getAttr();
        String actualStr = actualAttr.getAttrString();

        //Assert
        assertEquals(expectedStr,actualStr);
    }

    //Test for ranger
    @Test
    public void testRangerLevelUpStats_levelRanger_returnEqualAttributes(){
        //Arrange
        PlayerCharacter ranger = new PlayerCharacter("Legolas", CharacterClass.ClassChoices.RANGER, 1);
        String expectedStr = "Str: "+2+"\n"+"Dex: "+12+"\n"+"Int: "+2+"\n"+"Vit: "+1;

        //Act
        ranger.levelUp();
        Attributes actualAttr = ranger.getAttr();
        String actualStr = actualAttr.getAttrString();

        //Assert
        assertEquals(expectedStr,actualStr);
    }

    //Test for rogue
    @Test
    public void testRogueLevelUpStats_levelRogue_returnEqualAttributes(){
        //Arrange
        PlayerCharacter rogue = new PlayerCharacter("Pippin", CharacterClass.ClassChoices.ROGUE, 1);
        String expectedStr = "Str: "+3+"\n"+"Dex: "+10+"\n"+"Int: "+2+"\n"+"Vit: "+1;

        //Act
        rogue.levelUp();
        Attributes actualAttr = rogue.getAttr();
        String actualStr = actualAttr.getAttrString();

        //Assert
        assertEquals(expectedStr,actualStr);
    }

    //Test for warrior
    @Test
    public void testWarriorLevelUpStats_levelWarrior_returnEqualAttributes(){
        //Arrange
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);
        String expectedStr = "Str: "+8+"\n"+"Dex: "+4+"\n"+"Int: "+2+"\n"+"Vit: "+1;

        //Act
        warrior.levelUp();
        Attributes actualAttr = warrior.getAttr();
        String actualStr = actualAttr.getAttrString();

        //Assert
        assertEquals(expectedStr,actualStr);
    }

    //Checking if base DPS is calculated correctly
    @Test
    public void testUnequippedLevelOneWarrior_createWarrior_returnBaseDPS(){
        //Arrange
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);
        double expected = 1*(1 + 0.05);

        //Act
        double actual = warrior.getDPS();

        //Assert
        assertEquals(expected,actual);
    }

    //Checking if DPS with weapon is calculated correctly
    @Test
    public void testWarriorWithWeapon_createWarrior_returnDPS(){
        //Arrange
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);
        Weapon testAxe = new Weapon("Common Axe", 1,
                new Attributes(0,0,0,0), Item.Slot.WEAPON, 7,1.1, Weapon.WeaponType.AXES);
        double expected =  (7 * 1.1)*(1 + 0.05);

        //Act
        warrior.equipItem(testAxe);
        double actual = warrior.getDPS();

        //Assert
        assertEquals(expected,actual);
    }

    //Checking if DPS with weapon and armor is calculated correctly
    @Test
    public void testWarriorWithWeaponAndArmor_createWarrior_returnDPS(){
        //Arrange
        PlayerCharacter warrior = new PlayerCharacter("Gimli", CharacterClass.ClassChoices.WARRIOR, 1);
        Weapon testAxe = new Weapon("Common Axe", 1,
                new Attributes(0,0,0,0), Item.Slot.WEAPON, 7,1.1, Weapon.WeaponType.AXES);
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1,
                new Attributes(1,0,0,2), Item.Slot.BODY, Armor.ArmorType.PLATE);

        double expected =  (7 * 1.1) * (1 + (0.06));

        //Act
        warrior.equipItem(testAxe);
        warrior.equipItem(testPlateBody);
        double actual = warrior.getDPS();

        //Assert
        assertEquals(expected,actual);
    }

}